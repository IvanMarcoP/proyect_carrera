using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaser_Controller : MonoBehaviour
{
    public float radius;
    public float speed;
    private Transform objetivo;
    public float daņoPorSegundo = 10f;
    public float tiempoVida = 15f;
    public float activationDelay = 2f;
    bool isActive;
    private bool enContactoConJugador = false;
    private bool enContactoConJugador2 = false;

    // Start is called before the first frame update
    void Start()
    {
        SphereCollider detector = gameObject.AddComponent<SphereCollider>();
        detector.isTrigger = true;
        detector.radius = radius;
        Invoke("Activate", activationDelay);

        Destroy(gameObject, tiempoVida);
    }

    // Update is called once per frame
    void Update()
    {
        if (objetivo != null && isActive)
        {
            // Seguir al objetivo
            Vector3 direccion = (objetivo.position - transform.position).normalized;
            transform.position += direccion * speed * Time.deltaTime;

            if (enContactoConJugador)
            {
                Health_Controller healthController = objetivo.GetComponent<Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(daņoPorSegundo * Time.deltaTime);
                }
            }
            else if (enContactoConJugador2)
            {
                P2Health_Controller healthController2 = objetivo.GetComponent<P2Health_Controller>();
                if (healthController2 != null)
                {
                    healthController2.ApplyDamage(daņoPorSegundo * Time.deltaTime);
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (isActive && other.CompareTag("Player"))
        {
            objetivo = other.transform;
            enContactoConJugador = true;
        }
        else if (isActive && other.CompareTag("Player2"))
        {
            objetivo = other.transform;
            enContactoConJugador2 = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objetivo = null;
            enContactoConJugador = false;
        }
        else if (other.CompareTag("Player2"))
        {
            objetivo = null;
            enContactoConJugador2 = false;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red; // Color del gizmo
        Gizmos.DrawWireSphere(transform.position, radius); // Dibujar el radio
    }

    void Activate()
    {
        isActive = true;
    }

}
