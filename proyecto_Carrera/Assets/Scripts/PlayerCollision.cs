using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public float energyRecovered = 20f;
    public float healthRecovered = 20f;
    public float energyDrainAmount = 50f;
    public CarController CC;
    public Health_Controller HC;
    public UseItemController IC;
    public Transform respawnPosition;

    public GameObject[] items;

    void Start()
    {
        //HC = GetComponent<Health_Controller>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object"))
        {
            CC.AddEnergy(energyRecovered);
        }
        else if (other.CompareTag("Botiquin"))
        {
            HC.HealCar(healthRecovered);
        }
        else if (other.CompareTag("Shield"))
        {
            HC.ActivateShield(true);
        }
        else if (other.CompareTag("ItemBox"))
        {
            GameObject randomItem = GetRandomItem();

            ActivateItem(randomItem);
        }
        else if (other.CompareTag("Fuera"))
        {
            transform.position = respawnPosition.position;
        }
    }

    private GameObject GetRandomItem()
    {
        int randomIndex = Random.Range(0, items.Length);

        return items[randomIndex];
    }

    private void ActivateItem(GameObject item)
    {
        if (item.CompareTag("BombaPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.bombaItem = true;
        }
        else if (item.CompareTag("MisilPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.misilItem = true;
        }
        else if (item.CompareTag("MinaPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.mineItem = true;
            IC.currentMines = IC.maxMines;
        }
        else if(item.CompareTag("ObjetoDestruible") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.wallItem = true;
        }
        else if (item.CompareTag("FakeItem") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.fakeItem = true;
        }
        else if (item.CompareTag("PoisonPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.poisonItem = true;
        }
        else if(item.CompareTag("EnergyDrainerItem") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.energyDrainerItem = true;
        }
        else if (item.CompareTag("EnemyChaserPreview") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.enemyChaserItem = true;
        }
    }
}
