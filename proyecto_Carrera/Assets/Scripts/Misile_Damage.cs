using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Misile_Damage : MonoBehaviour
{
    public GameObject explosionPrefab;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public float dpsMisil = 100f;
    bool isActive;
    public float activationDelay = 2f;

    void ActivateMisil()
    {
        isActive = true;
    }

    void Start()
    {
        Invoke("ActivateMisil", activationDelay);
        Invoke("DestruirBala", 3);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

    //    if (other.CompareTag("Wall"))
    //    {
    //        DestruirBala();
    //        AplicarDaņoMisil();
    //    }

    //    if (isActive == true && other.CompareTag("Player") || isActive == true && other.CompareTag("Player2"))
    //    {
    //        AplicarDaņoMisil();
    //        DestruirBala();
    //    }
    //}

    void OnCollisionEnter(Collision collision)
    {
        if (isActive && collision.collider.CompareTag("Wall"))
        {
            AplicarDaņoMisil();
            Destroy(gameObject);
        }

        if (isActive && (collision.collider.CompareTag("Player") || collision.collider.CompareTag("Player2")))
        {
            AplicarDaņoMisil();
            Destroy(gameObject);
        }
    }

    private void AplicarDaņoMisil()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosion);
            }

            // Apply damage to enemies
            if (nearbyObject.CompareTag("Player"))
            {
                Health_Controller healthController = nearbyObject.GetComponent<Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(dpsMisil);
                }
            }
            else if (nearbyObject.CompareTag("Player2"))
            {
                P2Health_Controller healthController = nearbyObject.GetComponent<P2Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(dpsMisil);
                }
            }
        }
        Vector3 explosionPosition = transform.position;
        Instantiate(explosionPrefab, explosionPosition, Quaternion.identity);
    }

    private void DestruirBala()
    {
        Destroy(gameObject);
        AplicarDaņoMisil();
    }
}
