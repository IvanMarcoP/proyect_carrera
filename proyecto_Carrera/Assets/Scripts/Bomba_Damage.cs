using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba_Damage : MonoBehaviour
{

    public GameObject explosionPrefab;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public float dpsBomba = 100f;
    bool isActive;
    public float activationDelay = 2f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("ActivateBomba", activationDelay);
        Invoke("DestruirBala", 3);
    }

    void ActivateBomba()
    {
        isActive = true;
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

    //    if (other.CompareTag("Wall"))
    //    {
    //        Destroy(gameObject);
    //        DetonarGranada();
    //    }

    //    if (isActive == true && other.CompareTag("Player") || isActive == true && other.CompareTag("Player2"))
    //    {
    //        DetonarGranada();
    //        Destroy(gameObject);
    //    }
    //}

    void OnCollisionEnter(Collision collision)
    {
        //if (collision.collider.CompareTag("Wall"))
        //{
        //    Destroy(gameObject);
        //    DetonarGranada();
        //}

        if (isActive && (collision.collider.CompareTag("Player") || collision.collider.CompareTag("Player2")))
        {
            DetonarGranada();
            Destroy(gameObject);
        }
    }

    private void DetonarGranada()
    {
        // Obtener todas las colisiones en el radio de explosi�n
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        // Aplicar da�o o efectos a los objetos afectados
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Player"))
            {
                Health_Controller healthController = collider.GetComponent<Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(dpsBomba);
                }
            }
            else if (collider.CompareTag("Player2"))
            {
                P2Health_Controller healthController = collider.GetComponent<P2Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(dpsBomba);
                }
            }
            else
            {
                Rigidbody rb = collider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    // Aplicar fuerza explosiva a los objetos con Rigidbody
                    rb.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosion);
                }
            }
        }


        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void DestruirBala()
    {
        Destroy(gameObject);
        DetonarGranada();
    }
}
