using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPickUp : MonoBehaviour
{
    public GameObject barrierPrefab;
    public GameObject shieldPrefab;
    public float respawnTime = 3f;
    [SerializeField] private float timer = 0f;
    private bool activate = false;

    private BoxCollider boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    void Update()
    {
        if (activate)
        {
            timer += Time.deltaTime;
            if (timer >= respawnTime)
            {
                shieldPrefab.SetActive(true);
                activate = false;
                boxCollider.enabled = true;
                timer = 0f;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Player2"))
        {
            activate = true;
            boxCollider.enabled = false;
            GameObject jugador = other.gameObject;

            shieldPrefab.SetActive(false);
        }
    }
}
