using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUpTimer : MonoBehaviour
{
    public GameObject prefab;

    public float respawnTime = 5f;
    [SerializeField] private float timer = 0f;
    private bool activate = false;
    private BoxCollider boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    void Update()
    {
        if (activate)
        {
            timer += Time.deltaTime;
            if (timer >= respawnTime)
            {
                prefab.SetActive(true);
                activate = false;
                boxCollider.enabled = true;
                timer = 0f;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Player2"))
        {
            activate = true;
            boxCollider.enabled = false;
            prefab.SetActive(false);
        }
    }
}
