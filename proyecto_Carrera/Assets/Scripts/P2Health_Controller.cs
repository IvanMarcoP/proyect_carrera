using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class P2Health_Controller : MonoBehaviour
{
    public P2CarController CC;

    [Header("Life")]
    public Slider healthSlider;
    public float maxHealth = 100f;
    public float currentHealth;
    //public float healthDecDrift = 20f;
    public float healthDecMoving = 5f;
    public GameObject shield;
    public bool isShieldActive;

    [Header("Respawn Data")]
    //public CarController CC;
    public GameObject car;
    public GameObject sphere;
    public float respawnDelay = 2f;
    public Transform respawnPosition;

    [Header("Score")]
    public int muertesScore = 0;
    public int killsScore = 0;
    public TextMeshProUGUI muertesTxt;
    public TextMeshProUGUI killsTxt;

    bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        UpdateScore();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSliderHP();
        UpdateScore();

        if (killsScore >= 5)
        {
            GameManager.instance.p2Wins = true;
        }
    }

    void UpdateSliderHP()
    {
        healthSlider.value = currentHealth;

    }

    public void RecieveDamage(float amount)
    {
        ApplyDamage(amount);
    }

    public void ApplyDamage(float amount)
    {
        if(isDead) return;

        if (isShieldActive)
        {
            BreakShield();
        }
        else
        {
            currentHealth -= amount;
            healthSlider.value = currentHealth;

            if (currentHealth <= 0)
            {
                Die();
                Debug.Log("Muerto");
                // L�gica de muerte
            }
        }
    }

    public void Die()
    {
        if (isDead) return;

        isDead = true;
        car.SetActive(false);
        sphere.SetActive(false);
        muertesScore += 1;
        killsScore += 1;
        UpdateScore();

        StartCoroutine(Respawn());
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnDelay);

        transform.position = respawnPosition.position;
        transform.rotation = respawnPosition.rotation;

        currentHealth = maxHealth;
        isDead = false;
        UpdateSliderHP();
        CC.currentEnergy = CC.maxEnergy;

        car.SetActive(true);
        sphere.SetActive(true);
    }

    public void ActivateShield(bool estado)
    {
        isShieldActive = estado;
        if (shield != null)
        {
            shield.SetActive(estado);
        }
    }

    public void BreakShield()
    {
        isShieldActive = false;

        if (shield != null)
        {
            shield.SetActive(false);
        }
    }

    public void HealCar(float amount)
    {
        currentHealth += amount;

        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void UpdateScore()
    {
        muertesTxt.text = muertesScore.ToString();
        killsTxt.text = killsScore.ToString();
    }

    public void DecreaseEnergy(float amount)
    {
        CC.currentEnergy -= amount;
        CC.currentEnergy = Mathf.Clamp(CC.currentEnergy, 0f, CC.maxEnergy);
        //CC.UpdateSlider();
    }
}

//void UpdateWheels()
//{
//    int activeWheels = Mathf.CeilToInt((currentHealth / maxHealth) * maxWheels);

//    for (int i = 0; i < maxWheels; i++)
//    {
//        wheels[i].SetActive(i < activeWheels);
//    }

//    if (currentHealth <= damageThreshold)
//    {
//        DisableRandomWheel();
//    }
//}

//void DisableRandomWheel()
//{
//    int randomIndex = Random.Range(0, maxWheels);
//    GameObject wheel = wheels[randomIndex];

//    if (wheel.activeSelf)
//    {
//        // Desanclar la rueda del padre (el auto)
//        //wheel.transform.parent = null;

//        // Activar el Rigidbody de la rueda para que caiga
//        Rigidbody rb = wheel.GetComponent<Rigidbody>();
//        if (rb != null)
//        {
//            rb.isKinematic = false;

//            Vector3 randomDirection = Random.onUnitSphere;
//            float forceMagnitude = Random.Range(10f, 20f);
//            rb.AddForce(randomDirection * forceMagnitude, ForceMode.Impulse);
//        }
//    }
//}