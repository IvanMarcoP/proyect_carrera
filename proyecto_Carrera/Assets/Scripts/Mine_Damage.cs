using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine_Damage : MonoBehaviour
{
    public float activationDelay = 2f;
    public float proximityRadius = 5f;
    public float explotionForce = 100f;
    public float explotionRadius = 100f;
    public float mineDps = 50f;
    public GameObject explotionPrefab;
    bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("ActivateMine", activationDelay);
    }

    void ActivateMine() 
    { 
        isActive = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (isActive && (other.CompareTag("Player") || other.CompareTag("Player2")))
        {
            Explode();
        }
    }

    void Explode()
    {

        if (explotionPrefab != null)
        {
            Instantiate(explotionPrefab, transform.position, transform.rotation);
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position, explotionRadius);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explotionForce, transform.position, explotionRadius);
            }

            // Apply damage to enemies
            if (nearbyObject.CompareTag("Player"))
            {
                Health_Controller healthController = nearbyObject.GetComponent<Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(mineDps);
                }
            }
            else if (nearbyObject.CompareTag("Player2"))
            {
                P2Health_Controller healthController = nearbyObject.GetComponent<P2Health_Controller>();
                if (healthController != null)
                {
                    healthController.ApplyDamage(mineDps);
                }
            }
        }

        // Destroy the mine
        Destroy(gameObject);
    }
}
