using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonBomb_Controller : MonoBehaviour
{
    public float poisonDuration = 10f;
    public float explositionRadius = 5f;
    public GameObject poisonPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            Explode();
            Destroy(gameObject);
        }
    }

    private void Explode()
    {
        // Instanciar la nube de veneno en la posici�n de la bomba
        GameObject poisonCloud = Instantiate(poisonPrefab, transform.position, Quaternion.identity);
        //Destroy(poisonCloud, poisonDuration);
    }
}
