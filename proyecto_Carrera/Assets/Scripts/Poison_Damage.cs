using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison_Damage : MonoBehaviour
{
    public float dps = 10f;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Health_Controller healthController = other.GetComponent<Health_Controller>();
            if (healthController != null)
            {
                healthController.ApplyDamage(dps * Time.deltaTime);
            }
        }else if (other.CompareTag("Player2"))
        {
            P2Health_Controller healthController = other.GetComponent<P2Health_Controller>();
            if (healthController != null)
            {
                healthController.ApplyDamage(dps * Time.deltaTime);
            }
        }
    }
}
