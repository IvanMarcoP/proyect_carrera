using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPickUp_Controller : MonoBehaviour
{
    public GameObject energyPickUp;
    public float respawnTime = 5f;
    [SerializeField] private float timer = 0f;
    bool activate = false;
    private BoxCollider boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    void Update()
    {
        if(activate)
        {
            timer += Time.deltaTime;
            if (timer >= respawnTime)
            {
                energyPickUp.SetActive(true);
                activate = false;
                boxCollider.enabled = true;
                timer = 0f;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Player2"))
        {
            activate = true;
            energyPickUp.SetActive(false);
            boxCollider.enabled = false;
        }
           
    }
}
