using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool p1Wins;
    public TextMeshProUGUI p1WinsTxt;
    public bool p2Wins;
    public TextMeshProUGUI p2WinsTxt;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        Time.timeScale = 1f;
        p1Wins = false; p2Wins = false;
        p1WinsTxt.gameObject.SetActive(false);
        p2WinsTxt.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            restartScene();
        }

        if(p1Wins)
        {
            p1WinsTxt.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else if(p2Wins)
        {
            p2WinsTxt.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }

    private void restartScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(currentSceneIndex);
    }
}
