using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    public float maxHealth = 100f;
    public float energyRecovered = 20f;
    private float currentHealth;
    private bool isActive;
    public float activationDelay = 2f;

    void Start()
    {
        currentHealth = maxHealth;
        Invoke("ActivateWall", activationDelay);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter triggered by: " + other.gameObject.name);

        if (isActive)
        {
            if (other.CompareTag("Player"))
            {
                Health_Controller hc = other.GetComponent<Health_Controller>();
                if (hc != null)
                {
                    hc.ApplyDamage(25);
                    Destroy(gameObject);
                }
                else
                {
                    Debug.LogWarning("Health_Controller component not found on Player");
                }
            }
            else if (other.CompareTag("Player2"))
            {
                P2Health_Controller hc2 = other.GetComponent<P2Health_Controller>();
                if (hc2 != null)
                {
                    hc2.ApplyDamage(25);
                    Destroy(gameObject);
                }
                else
                {
                    Debug.LogWarning("P2Health_Controller component not found on Player2");
                }
            }
            else if (other.CompareTag("Misil"))
            {
                Misile_Damage missile = other.GetComponent<Misile_Damage>();
                if (missile != null)
                {
                    TakeDamage(missile.dpsMisil);
                    Destroy(other.gameObject);
                }
                else
                {
                    Debug.LogWarning("Misile_Damage component not found on Misil");
                }
            }
            else if (other.CompareTag("Bomba"))
            {
                Bomba_Damage bomba = other.GetComponent<Bomba_Damage>();
                if (bomba != null)
                {
                    TakeDamage(bomba.dpsBomba);
                    Destroy(other.gameObject);
                }
                else
                {
                    Debug.LogWarning("Bomba_Damage component not found on Bomba");
                }
            }
        }
    }

    void TakeDamage(float damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    void ActivateWall()
    {
        isActive = true;
    }
}
