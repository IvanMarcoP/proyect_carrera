using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2PlayerCollision : MonoBehaviour
{
    public float energyRecovered = 20f;
    public float healthRecovered = 20f;
    public P2CarController CC;
    public P2Health_Controller HC;
    public P2UseItemController IC;
    public Transform respawnPosition;

    public GameObject[] items;

    void Start()
    {
        HC = GetComponent<P2Health_Controller>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object"))
        {
            CC.AddEnergy(energyRecovered);
        }
        else if (other.CompareTag("Botiquin"))
        {
            HC.HealCar(healthRecovered);
        }
        else if (other.CompareTag("Shield"))
        {
            HC.ActivateShield(true);
        }
        else if (other.CompareTag("ItemBox"))
        {
            GameObject randomItem = GetRandomItem();

            ActivateItem(randomItem);
        }
        else if (other.CompareTag("Fuera"))
        {
            transform.position = respawnPosition.position;
        }
    }


    private GameObject GetRandomItem()
    {
        int randomIndex = Random.Range(0, items.Length);

        return items[randomIndex];
    }

    private void ActivateItem(GameObject item)
    {
        if (item.CompareTag("BombaPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.bombaItem = true;
        }
        else if (item.CompareTag("MisilPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.misilItem = true;
        }
        else if (item.CompareTag("MinaPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.mineItem = true;
            IC.currentMines = IC.maxMines;
        }
        else if (item.CompareTag("ObjetoDestruible") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.wallItem = true;
        }
        else if (item.CompareTag("FakeItem") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.fakeItem = true;
        }
        else if (item.CompareTag("PoisonPickUp") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.poisonItem = true;
        }
        else if (item.CompareTag("EnergyDrainerItem") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.energyDrainerItem = true;
        }
        else if (item.CompareTag("EnemyChaserPreview") && IC.pickItem == false)
        {
            IC.pickItem = true;
            IC.enemyChaserItem = true;
        }
    }
}
