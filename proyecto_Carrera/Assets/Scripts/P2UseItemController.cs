using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2UseItemController : MonoBehaviour
{
    [Header("Misil")]
    public GameObject misilPreview1;
    public GameObject misilPreview2;
    public GameObject misilPrefab;
    public float fuerzaLanzamiento;
    public Transform posicionLanzamiento1;
    public Transform posicionLanzamiento2;

    [Header("Bomba")]
    public GameObject bombaPreview;
    public GameObject bombaPrefab;
    public float fuerzaBomba;
    public Transform bombaPosicionLanzamiento;

    [Header("Mine")]
    public GameObject minePreView;
    public GameObject minePrefab;
    public Transform minePosicionLanzamiento;
    public int maxMines = 5;
    public int currentMines;

    [Header("Wall")]
    public GameObject wallPreview;
    public GameObject wallPrefab;
    public Transform wallPosition;

    [Header("FakeItem")]
    public GameObject fakePreview;
    public GameObject fakePrefab;
    public Transform fakePosition;

    [Header("PoisonItem")]
    public GameObject poisonPreview;
    public GameObject poisonPrefab;
    public float poisonFuerza;
    public Transform poisonPosition;

    [Header("EnergyDrainer")]
    public GameObject endPreview;
    public GameObject endPrefab;
    public Transform endPosition;

    [Header("EnemyChaser")]
    public GameObject enemyPreview;
    public GameObject enemyPrefab;
    public Transform enemyPosition;

    [Header("Bools")]
    public bool pickItem;
    public bool shieldItem;
    public bool misilItem;
    public bool bombaItem;
    public bool mineItem;
    public bool wallItem;
    public bool fakeItem;
    public bool poisonItem;
    public bool energyDrainerItem;
    public bool enemyChaserItem;

    // Start is called before the first frame update
    void Start()
    {
        currentMines = maxMines;
        DeActiveItemsStart();
    }

    // Update is called once per frame
    void Update()
    {
        ActivePreview();

        if (pickItem)
        {

            if (Input.GetButtonDown("FireGun") && misilItem == true || Input.GetKeyDown(KeyCode.Keypad2) && misilItem == true)
            {
                shootMisile();
                misilItem = false;
                pickItem = false;
            }
            else if (Input.GetButtonDown("FireGun") && bombaItem == true || Input.GetKeyDown(KeyCode.Keypad2) && bombaItem == true)
            {
                ShootBomba();
                bombaItem = false;
                pickItem = false;
            }
            else if (Input.GetButtonDown("FireGun") && mineItem == true || Input.GetKeyDown(KeyCode.Keypad2) && mineItem == true)
            {
                DeployMine();
                currentMines--;
                pickItem = false;
                mineItem = false;

            }
            else if (Input.GetButtonDown("FireGun") && wallItem == true || Input.GetKeyDown(KeyCode.Keypad2) && wallItem == true)
            {
                DeployWall();
                wallItem = false;
                pickItem = false;
            }
            else if (Input.GetButtonDown("FireGun") && fakeItem == true || Input.GetKeyDown(KeyCode.Keypad2) && fakeItem == true)
            {
                DeployFake();
                fakeItem = false;
                pickItem = false;
            }
            else if(Input.GetButtonDown("FireGun") && poisonItem == true || Input.GetKeyDown(KeyCode.Keypad2) && poisonItem == true)
            {
                ShootPoisonbomb();
                poisonItem = false;
                pickItem = false;
            }
            else if (Input.GetButtonDown("FireGun") && energyDrainerItem == true || Input.GetKeyDown(KeyCode.Keypad2) && energyDrainerItem == true)
            {
                DeployEnergyDrainer();
                energyDrainerItem = false;
                pickItem = false;
            }
            else if (Input.GetButtonDown("FireGun") && enemyChaserItem == true || Input.GetKeyDown(KeyCode.Keypad2) && enemyChaserItem == true)
            {
                DeployEnemyChaser();
                enemyChaserItem = false;
                pickItem = false;
            }
        }
    }

    void ActivePreview()
    {
        CheckAndSetActive("misilItem", misilItem, new GameObject[] { misilPreview1, misilPreview2 });
        CheckAndSetActive("bombaItem", bombaItem, new GameObject[] { bombaPreview });
        CheckAndSetActive("mineItem", mineItem, new GameObject[] { minePreView });
        CheckAndSetActive("wallItem", wallItem, new GameObject[] { wallPreview });
        CheckAndSetActive("fakeItem", fakeItem, new GameObject[] { fakePreview });
        CheckAndSetActive("poisonItem", poisonItem, new GameObject[] { poisonPreview });
        CheckAndSetActive("energyDrainerItem", energyDrainerItem, new GameObject[] { endPreview });
        CheckAndSetActive("enemyChaserItem", enemyChaserItem, new GameObject[] { enemyPreview });
    }

    void CheckAndSetActive(string itemName, bool isActive, GameObject[] previews)
    {
        switch (itemName)
        {
            case "misilItem":
            case "bombaItem":
            case "mineItem":
            case "wallItem":
            case "fakeItem":
            case "poisonItem":
            case "energyDrainerItem":
            case "enemyChaserItem":
                foreach (var preview in previews)
                {
                    preview.SetActive(isActive);
                }
                break;
            default:
                Debug.LogError("Item name not recognized");
                break;
        }
    }

    void DeActiveItemsStart()
    {
        misilPreview1.SetActive(false);
        misilPreview2.SetActive(false);
        bombaPreview.SetActive(false);
        minePreView.SetActive(false);
        wallPreview.SetActive(false);
        fakePreview.SetActive(false);
        poisonPreview.SetActive(false);
        endPreview.SetActive(false);
        enemyPreview.SetActive(false);
    }

    void shootMisile()
    {
        GameObject misil1 = Instantiate(misilPrefab, posicionLanzamiento1.position, transform.rotation);
        Rigidbody misilRb1 = misil1.GetComponent<Rigidbody>();

        GameObject misil2 = Instantiate(misilPrefab, posicionLanzamiento2.position, transform.rotation);
        Rigidbody misilRb2 = misil2.GetComponent<Rigidbody>();

        misilRb1.velocity = transform.forward * fuerzaLanzamiento * 100f;
        misilRb2.velocity = transform.forward * fuerzaLanzamiento * 100f;
    }

    void ShootBomba()
    {
        GameObject granada = Instantiate(bombaPrefab, bombaPosicionLanzamiento.position, transform.rotation);
        Rigidbody granadaRb = granada.GetComponent<Rigidbody>();

        Vector3 direccionLanzamiento = transform.forward;

        granadaRb.AddForce(direccionLanzamiento * fuerzaBomba, ForceMode.VelocityChange);
    }

    void DeployMine()
    {
        Instantiate(minePrefab, minePosicionLanzamiento.position, minePosicionLanzamiento.rotation);
    }

    void DeployWall()
    {
        Instantiate(wallPrefab, wallPosition.position, wallPosition.rotation);
    }

    void DeployFake()
    {
        Instantiate(fakePrefab, fakePosition.position, fakePosition.rotation);
    }

    void ShootPoisonbomb()
    {
        GameObject granada = Instantiate(poisonPrefab, poisonPosition.position, transform.rotation);
        Rigidbody granadaRb = granada.GetComponent<Rigidbody>();

        Vector3 direccionLanzamiento = -transform.forward;

        granadaRb.AddForce(direccionLanzamiento * poisonFuerza, ForceMode.VelocityChange);
    }

    void DeployEnergyDrainer()
    {
        Instantiate(endPrefab, endPosition.position, endPosition.rotation);
    }

    void DeployEnemyChaser()
    {
        Instantiate(enemyPrefab, enemyPosition.position, enemyPosition.rotation);
    }
}
