using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P2CarController : MonoBehaviour
{
    public Rigidbody esferaRB;
    public P2Health_Controller HC;
    [SerializeField] private Animator animator;

    [Header("Car Data")]
    public float forwardAccel = 8f;
    public float reverseAccel = 4f;
    //public float maxSpd = 50f;
    public float turnStrength = 180f;
    private float orinalTurnStrength;
    public float dragGround = 3f;
    public float gravity = 10f;
    public float jumpStrenght = 5f;
    public float raycastDistance = 5f;
    //public float flipForce = 20f;

    public LayerMask whatIsGround;
    public LayerMask whatIsWall;
    public Transform groundRayPoint;
    public float rayGroundDist = 0.5f;

    [Header("Drifting Data")]
    public bool drifting;
    public float driftDuration = 2f;
    public float driftingTurnStrenght = 150f;
    [SerializeField] private float driftTimer = 0f;
    [SerializeField] private float driftBoost = 10f;

    [Header("Energy Data")]
    public Slider energySlider;
    public float maxEnergy = 100f;
    public float energyDecDrift = 20f;
    public float energyDecMoving = 5f;
    public float energyFlip = 10f;
    //public float energyRecover = 20f;
    public float currentEnergy;
    [SerializeField] private float flipPoints = 0;

    [Header("Boost Data")]
    public float boostForce = 50f;
    public float boostDuration = 1f;
    public float boostEnergy = 20f;
    //public float delayInAnimation = 0.5f;

    private float speedInput, turnInput;
    [SerializeField] private bool turning;

    [SerializeField] private bool grounded;


    // Start is called before the first frame update
    void Start()
    {
        orinalTurnStrength = turnStrength;
        esferaRB.transform.parent = null;
        esferaRB.transform.localScale = new Vector3(3f, 3f, 3f);
        currentEnergy = maxEnergy;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSlider();
        CarMovement();
        ControlInputs();
        //FlipCar();
    }

    void FixedUpdate()
    {
        grounded = false;
        RaycastHit hit;

        if (Physics.Raycast(groundRayPoint.position, -transform.up, out hit, rayGroundDist, whatIsGround))
        {
            grounded = true;

            transform.rotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
        }
        else
        {
            grounded = false;
        }


        if (grounded)
        {
            esferaRB.drag = dragGround;


            if (Mathf.Abs(speedInput) > 0)
            {
                esferaRB.AddForce(transform.forward * speedInput);
            }

        }
        else
        {
            esferaRB.drag = 0.1f;
            esferaRB.AddForce(Vector3.up * -gravity * 100f);
        }
    }

    void CarMovement()
    {
        speedInput = 0f;

        //movimiento
        if (Input.GetAxis("VerticalControl") > 0&& currentEnergy > 0 && grounded)
        {
            RaycastHit hit;
            float sphereRadius = 0.5f;
            if (!Physics.SphereCast(transform.position, sphereRadius, transform.forward, out hit, raycastDistance, whatIsWall))
            {
                // Si no hay colisi�n, permitir el movimiento hacia adelante
                speedInput = Input.GetAxis("VerticalControl") * forwardAccel * 1000;
                currentEnergy -= energyDecMoving * Time.deltaTime;
                currentEnergy = Mathf.Max(0, currentEnergy);
                energySlider.value = currentEnergy;
            }
        }
        else if (Input.GetAxis("VerticalControl") < 0)
        {
            speedInput = Input.GetAxis("VerticalControl") * reverseAccel * 1000f;
        }
        else
        {
            speedInput = 0;
        }

        if (currentEnergy == 0)
        {
            speedInput = Input.GetAxis("VerticalControl") * forwardAccel * 500f;
            //HC.HpDrifting();
        }

        turnInput = Input.GetAxis("HorizontalControl");
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0f, turnInput * turnStrength * Time.deltaTime, 0f));

        if (turnInput < 0 || turnInput > 0)
        {
            turning = true;
        }
        else
        {
            turning = false;
        }
    }

    void ControlInputs()
    {
        //iniciar drift
        if (Input.GetButtonDown("Drift") && turning || Input.GetKeyDown(KeyCode.RightShift) && turning)
        {
            drifting = true;
        }
        //cancelar drift
        if (Input.GetButtonUp("Drift") || Input.GetKeyUp(KeyCode.RightShift) || currentEnergy <= 0)
        {
            drifting = false;
            turnStrength = orinalTurnStrength;

            if (driftTimer >= driftDuration)
            {
                Boost();
                driftTimer = 0f;
            }
            else
            {
                driftTimer = 0f;
            }
        }

        //Perder energia mientras hago Drifting
        if (drifting && currentEnergy > 0f)
        {
            driftTimer += Time.deltaTime;

            turnStrength = driftingTurnStrenght;
            currentEnergy -= energyDecDrift * Time.deltaTime;
            currentEnergy = Mathf.Max(0, currentEnergy);
            energySlider.value = currentEnergy;
        }


        transform.position = esferaRB.transform.position;


        //Boost Atack
        //if (Input.GetButtonDown("Boost") && currentEnergy >= 20 && Input.GetAxis("VerticalControl") > 0 || Input.GetKeyDown(KeyCode.Keypad1) && currentEnergy >= 20 && Input.GetAxis("VerticalControl") > 0)
        //{
        //    StartCoroutine(BoostAtack());
        //    currentEnergy = currentEnergy - boostEnergy;
        //    energySlider.value = currentEnergy;
        //}

        ////Salto
        //if (Input.GetButtonDown("JumpControl") && grounded || Input.GetKeyDown(KeyCode.Keypad0) && grounded)
        //{
        //    esferaRB.AddForce(Vector3.up * jumpStrenght * 1000f);
        //}
    }

    void UpdateSlider()
    {
        energySlider.value = currentEnergy;
    }

    public void Boost()
    {
        esferaRB.AddForce(transform.forward * driftBoost, ForceMode.VelocityChange);
    }

    private IEnumerator BoostAtack()
    {
        float originalSpd = forwardAccel;

        forwardAccel += boostForce;

        yield return new WaitForSeconds(boostDuration);

        forwardAccel = originalSpd;
    }

    void FlipCar()
    {


        if (!animator.GetBool("FrontPressed") && Input.GetAxis("VerticalFlip") > 0 && !grounded)
        {
            //animator.SetBool("FrontPressed", true);
            animator.Play("P2FrontFlip");
            AddFlipPoints(1);
        }
        else if (!animator.GetBool("FrontPressed") && Input.GetAxis("VerticalFlip") < 0 && !grounded)
        {
            animator.Play("P2BackFlip");
            AddFlipPoints(1);
        }
        else if (!animator.GetBool("FrontPressed") && Input.GetAxis("HorizontalFlip") > 0 && !grounded)
        {
            animator.Play("P2RightFlip");
            AddFlipPoints(1);
        }
        else if (!animator.GetBool("FrontPressed") && Input.GetAxis("HorizontalFlip") < 0 && !grounded)
        {
            animator.Play("P2LeftFlip");
            AddFlipPoints(1);
        }

        if (grounded)
        {
            AddFlipEnergy();
            animator.SetBool("FrontPressed", false);
        }
    }

    public void AddEnergy(float energyToAdd)
    {
        currentEnergy += energyToAdd;
        currentEnergy = Mathf.Clamp(currentEnergy, 0f, maxEnergy);
        UpdateSlider();
    }

    void AddFlipEnergy()
    {
        currentEnergy += (flipPoints * energyFlip) / 10;
        currentEnergy = Mathf.Min(currentEnergy, maxEnergy);
        flipPoints = 0;
    }

    void AddFlipPoints(float points)
    {
        flipPoints += points;
    }

    public void DecreaseEnergy(float amount)
    {
        currentEnergy -= amount;
        currentEnergy = Mathf.Clamp(currentEnergy, 0f, maxEnergy);
        UpdateSlider();
    }
}
