using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyDrain_Controller : MonoBehaviour
{
    public float energyDrainAmount = 50f;
    bool isActive;
    public float activationDelay = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("ActivateBomba", activationDelay);
    }

    void ActivateBomba()
    {
        isActive = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (isActive && other.CompareTag("Player"))
        {
            // Obtenemos el componente Health_Controller del jugador
            Health_Controller healthController = other.GetComponent<Health_Controller>();
            if (healthController != null)
            {
                // Llamamos al m�todo para drenar energ�a
                healthController.DecreaseEnergy(energyDrainAmount);
                Destroy(gameObject);
                Debug.Log("Se dreno energia");
            }
        }
        else if (isActive && other.CompareTag("Player2"))
        {
            P2Health_Controller healthController = other.GetComponent<P2Health_Controller>();
            if (healthController != null)
            {
                // Llamamos al m�todo para drenar energ�a
                healthController.DecreaseEnergy(energyDrainAmount);
                Destroy(gameObject);
            }
        }
    }
}
